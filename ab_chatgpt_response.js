// 读取原始响应体
var body = JSON.parse($response.body);

// 修改 feature_gates 中的值
let featureGates = body["feature_gates"];
for (const key in featureGates) {
    if (featureGates[key].value === false) {
        featureGates[key].value = true;
    }
}
body["feature_gates"] = featureGates;

// 返回修改后的响应体
$done({body: JSON.stringify(body)});